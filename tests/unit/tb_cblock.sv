`include "vunit_defines.svh"
`include "cgate.v"

`timescale 1ns / 1ps

module tb_CBlock;
    logic rst, i_req, o_ack;
    wire i_ack;

    CBlock c (.*);

    task test(logic x, y, res);
        i_req = x;
        o_ack = y;
        #1 `CHECK_EQUAL(i_ack, res);
    endtask

    `TEST_SUITE begin
        `TEST_CASE_SETUP begin
            i_req = 0;
            o_ack = 0;
            rst = 0;
            #1;
        end

        `TEST_CASE("reset") begin
            rst = 1;
            #1 rst = 0;
            #1 `CHECK_EQUAL(i_ack, 0);
        end

        `TEST_CASE("behaviour") begin
            test(0, 1, 0);
            test(1, 1, 0);
            test(0, 0, 0);
            test(1, 0, 1);
            test(1, 1, 1);
            test(0, 0, 1);
            test(0, 1, 0);
        end
    end
endmodule
