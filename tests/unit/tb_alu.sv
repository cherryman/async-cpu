`timescale 1ns / 1ps

`include "vunit_defines.svh"
`include "alu.v"

module tb_ALU;
    parameter XLEN = 32;

    logic rst;
    logic [XLEN-1:0] a, b;
    logic is_sub;
    logic i_req;
    wire o_ack;

    wire [XLEN-1:0] o;
    wire o_req, i_ack;

    integer n, m, res;

    assign o_ack = o_req;

    ALU #(XLEN, 1) alu (.*);

    task test(integer x, y, bit sub);
        #1 i_req = 0;
        wait(~o_req);

        a = x;
        b = y;
        is_sub = sub;

        res = sub ? x - y : x + y;

        #1 i_req = 1;
        wait (o_req);

        `CHECK_EQUAL(o, res);
    endtask

    `TEST_SUITE begin
        `TEST_CASE_SETUP begin
            i_req = 0;
            rst = 1;
            #1 rst = 0;
        end

        `TEST_CASE("behaviour") begin
            test(1, 1, 0);
            test(1, 1, 1);
            test(1, 0, 1);

            for (n = 0; n < 50; ++n) begin
                test($urandom(), $urandom(), 0);
                test($urandom(), $urandom(), 1);
            end
        end
    end
endmodule
