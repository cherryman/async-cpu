`include "delay.v"

module ROM
 #( parameter DELAY = 0
 )( input [9:0] pc
  , output reg [15:0] inst

  , input i_req, o_ack
  , output o_req, i_ack
  );

    assign i_ack = o_ack;
    Delay #(DELAY) d (o_req, i_req);

    always @* begin
        (* parallel_case *)
        case (pc)
            `include "rom.program.v"
        endcase
    end
endmodule
