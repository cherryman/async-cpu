`ifndef delay_v
`define delay_v

module Delay1(o, i);
    input i;

`ifdef SYNTHESIS
    // Forces synthesis of an LUT1 on Xilinx
    output o;
    (* KEEP = "TRUE" *) wire v, w;
    buf b1(v, i), b2(w, v), b3(o, w);
`else
    output reg o;
    always @* o = #1 i;
`endif

endmodule

module Delay #(parameter N = 1)(output o, input i);
    wire [N:0] w;
    assign w[0] = i;
    assign o = w[N];

    genvar k;
    for (k = 0; k < N; k = k + 1)
        Delay1 d (.i(w[k]), .o(w[k+1]));
endmodule

`endif
