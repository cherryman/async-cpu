`include "adder.v"
`include "cgate.v"
`include "delay.v"

module ALU
 #( parameter XLEN = 16
  , parameter DELAY = 0
 )( input rst
  , input [XLEN-1:0] a, b
  , output [XLEN-1:0] o

  , input is_sub

  , input i_req, o_ack
  , output o_req, i_ack
 );

    (* ASYNC_REG = "TRUE" *)
    reg [XLEN-1:0] _o;

    wire adder_i_req, adder_o_req;
    wire [XLEN-1:0] s;

    assign o = _o;

    always @(posedge i_ack)
        _o <= s;

    Delay #(DELAY) d_adder (adder_i_req, i_req);

    Adder #(XLEN) adder (
        .i_req(adder_i_req), .o_req(adder_o_req),

        .a(a), .b(b ^ {XLEN{is_sub}}), .s(s),
        .ci(is_sub), .co()
    );

    CBlock c (
        .i_req(adder_o_req), .i_ack(i_ack),
        .o_ack(o_ack),
        .rst(rst)
    );

    Delay #(DELAY) d (o_req, i_ack);
endmodule
