`include "cgate.v"
`include "delay.v"
`include "rom.v"
`include "dec.v"
`include "regfile.v"
`include "alu.v"
`include "mem.v"

module CPU
 #( parameter XLEN = 16
  , parameter RLEN = 3 // register addr length
  , parameter ALEN = 6 // memory addr length
 )( input clk
  , input rst
  , input [15:0] io_i
  , output [15:0] io_o
  );
    initial begin
        $monitor("PC %d -> %d %b i:%b o:%b",
            pc, pc_next, inst, io_i, io_o);
    end

    (* ASYNC_REG = "TRUE" *)
    reg [9:0] pc;

    wire [2:0] rs1, rs2, rd;
    wire [15:0] inst, imm;
    wire [XLEN-1:0] rs1_o, rs2_o, rd_i;
    wire [XLEN-1:0] alu_o;
    wire [XLEN-1:0] mem_o;
    wire [9:0] jmp_addr;
    wire [ALEN-1:0] mem_addr;
    wire rd_w, is_sub, is_jeq, is_jlt, is_jgt;
    wire rd_alu, rd_mem, mem_w;

    wire nez = |rs1_o;
    wire eqz = ~nez;
    wire ltz = rs1_o[XLEN-1];
    wire gtz = nez & ~ltz;

    wire do_jmp = (is_jeq & eqz) | (is_jlt & ltz) | (is_jgt & gtz);
    wire [9:0] pc_inc = pc + 1;
    wire [9:0] pc_next = do_jmp ? jmp_addr : pc_inc;

    assign rd_i = rd_alu ? alu_o : (rd_mem ? mem_o : imm);

    wire
        req_pc, ack_pc,
        req_rom, ack_rom,
        req_dec, ack_dec,
        req_r_regs, ack_r_regs,
        req_alu, ack_alu,
        req_mem, ack_mem,
        req_w_regs, ack_w_regs;

    // Start pulse generation
    wire start = ~rst;
    wire start_end;
    wire pulse = start & ~start_end;

    Delay #(1) start_d (start_end, start);

    // PC latch handling
    CBlock pc_c (
        .i_req(req_pc), .i_ack(ack_pc),
        .o_ack(ack_rom), .rst(rst)
    );
    Delay #(1) pc_d (req_rom, ack_pc);

    always @(posedge ack_pc, posedge rst)
        pc <= rst ? 0 : pc_next;

    ROM #(1) rom (
        .i_req(req_rom | pulse), .i_ack(ack_rom),
        .o_req(req_dec), .o_ack(ack_dec),

        .pc(pc), .inst(inst)
    );

    Dec #(1) dec (
        .i_req(req_dec), .i_ack(ack_dec),
        .o_req(req_r_regs), .o_ack(ack_r_regs),

        .inst(inst), .imm(imm),
        .mem_addr(mem_addr), .jmp_addr(jmp_addr),
        .rs1(rs1), .rs2(rs2), .rd(rd),
        .rd_w(rd_w), .is_sub(is_sub), .mem_w(mem_w),
        .rd_alu(rd_alu), .rd_mem(rd_mem),
        .is_jeq(is_jeq), .is_jlt(is_jlt), .is_jgt(is_jgt)
    );

    wire req_fork, ack_fork;
    wire req_o_alu, req_o_mem, ack_o_alu, ack_o_mem;
    wire r1, r2;

    RegFile #(XLEN, RLEN, 2) regs (
        .i_r_req(req_r_regs), .i_r_ack(ack_r_regs),
        .o_r_req(req_fork), .o_r_ack(ack_fork),

        .i_w_req(req_w_regs), .i_w_ack(ack_w_regs),
        .o_w_req(req_pc), .o_w_ack(ack_pc),

        .rs1(rs1), .rs2(rs2), .rd(rd),
        .rs1_o(rs1_o), .rs2_o(rs2_o), .rd_i(rd_i),
        .rd_w(rd_w), .rst(rst)
    );

    // fork
    CGate cfmem (.rst(rst), .a(req_fork), .b(~rd_alu & ~ack_fork), .y(req_mem));
    CGate cfalu (.rst(rst), .a(req_fork), .b(rd_alu & ~ack_fork), .y(req_alu));
    assign ack_fork = ack_mem | ack_alu;

    // join
    CGate cjmem1 (.rst(rst), .a(~rd_alu & ~ack_o_mem), .b(req_o_mem), .y(r1));
    CGate cjalu1 (.rst(rst), .a(rd_alu & ~ack_o_alu), .b(req_o_alu), .y(r2));
    assign req_w_regs = r1 | r2;
    CGate cjmem2 (.rst(rst), .a(r1), .b(ack_w_regs), .y(ack_o_mem));
    CGate cjalu2 (.rst(rst), .a(r2), .b(ack_w_regs), .y(ack_o_alu));

    Mem #(XLEN, ALEN, 1) mem (
        .i_req(req_mem), .i_ack(ack_mem),
        .o_req(req_o_mem), .o_ack(ack_o_mem),

        .io_i(io_i), .io_o(io_o),

        .d_i(rs1_o), .d_o(mem_o),
        .addr(mem_addr), .mem_w(mem_w),
        .rst(rst)
    );

    ALU #(XLEN, 1) alu (
        .i_req(req_alu), .i_ack(ack_alu),
        .o_req(req_o_alu), .o_ack(ack_o_alu),

        .a(rs1_o), .b(rs2_o),
        .is_sub(is_sub), .o(alu_o),
        .rst(rst)
    );
endmodule
