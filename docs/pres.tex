\documentclass[aspectratio=169]{beamer}
\mode<presentation>

\usepackage[backend=bibtex8]{biblatex}
\usepackage{tikz}
\usetikzlibrary{arrows.meta,positioning,circuits.logic.US}

\addbibresource{docs/refs.bib}

\usefonttheme{serif}
\setbeamerfont{title}{shape=\itshape}
\setbeamerfont{block title}{shape=\itshape}
\setbeamercolor{title}{fg=black}
\setbeamercolor{frametitle}{fg=black}
\setbeamercolor{block title}{fg=black}
\setbeamertemplate{itemize item}{\color{black}$\bullet$}

\author{Sheheryar Parvaz}
\date{April 16, 2020}
\title{Behavioural Simulation of an Asynchronous CPU}

\begin{document}
  \begin{frame}
    \titlepage
  \end{frame}

  \section{What is a CPU?}
  \begin{frame}[fragile]{What does a CPU do?}
    \begin{columns}
      \column{.4\textwidth}
      \begin{verbatim}
      0: li r1, 1
      1: li r2, 1
      2: add r0, r1, r2
      3: jgt r0, 0
      \end{verbatim}

      \column{.2\textwidth}
      \centering
      \begin{tikzpicture}
        \draw[thick,<->] (0, 0) -- (1, 0);
      \end{tikzpicture}

      \column{.4\textwidth}
        while $r_0 > 0$: \\
        \hspace{2em}$r_1 \leftarrow 1$ \\
        \hspace{2em}$r_2 \leftarrow 1$ \\
        \hspace{2em}$r_0 \leftarrow r_1 + r_2$ \\

    \end{columns}
  \end{frame}

  \begin{frame}{The CPU Cycle}
    \centering
    \begin{tikzpicture}[
      state/.style={circle,draw=black,font=\tiny,
        minimum width=2cm,minimum height=2cm,
        text width=1.75cm,align=center}
    ]
      \node (clk) {Clock};

      \node[state] (inst) [below=of clk]{Fetch\\Instruction};
      \node[state] (dec) [right=of inst] {Decode};
      \node[state] (exec) [right=of dec] {Execute};

      \draw[->] (clk.south) -- (inst.north);
      \draw[->] (clk.east) ..controls +(right:2cm).. (dec.north);
      \draw[->] (clk.east) ..controls +(right:5cm).. (exec.north);

      \draw[->,thick] (inst.east) -- (dec.west);
      \draw[->,thick] (dec.east) -- (exec.west);
      \draw[->,thick] (exec.south)
        .. controls +(down:2cm) and +(down:2cm).. (inst.south);
    \end{tikzpicture}
  \end{frame}

  \begin{frame}{Problems}
      \pause
    \begin{itemize}
      \item Wiring the clock can be difficult\pause
      \item The clock consumes power\pause
      \item Speed is limited to clock period
    \end{itemize}
  \end{frame}

  \section{Asynchronous CPUs}
  \begin{frame}{Project: Asynchronous CPU}

    \pause
    {\bf Asynchronous Circuit:}
    \pause
    A circuit which isn't governed by a clock. \pause
    Often uses wire propagation delays.\\

    \vspace{2em}

    \pause
    {\bf Behavioural Simulation:}
    \pause
    Simulate the behaviour of a physical circuit in software.

  \end{frame}

  \begin{frame}{Project: Asynchronous CPU}{Challenges}
    \pause
    \begin{itemize}
      \item Lack of research\pause
      \item Lack of tooling\pause
      \item Increased complexity
    \end{itemize}
  \end{frame}

  \begin{frame}{Project: Asynchronous CPU}{Revised CPU Cycle}
    \centering
    \begin{tikzpicture}[
      state/.style={circle,draw=black,font=\tiny,
        minimum width=2cm,minimum height=2cm,
        text width=1.75cm,align=center}
    ]
      \node[state] (inst) {Fetch\\Instruction};
      \node[state] (dec) [right=of inst] {Decode};
      \node[state] (exec) [right=of dec] {Execute};

      \draw[<->,thick] (inst.east) -- (dec.west)
        node[midway,above,font=\tiny]{req/ack};
      \draw[<->,thick] (dec.east) -- (exec.west)
        node[midway,above,font=\tiny]{req/ack};
      \draw[<->,thick] (exec.south)
        .. controls +(down:2cm) and +(down:2cm).. (inst.south)
        node[midway,above,font=\tiny]{req/ack};
    \end{tikzpicture}
  \end{frame}

  \begin{frame}{The Muller C-Element}
    \begin{columns}

      \column{.4\textwidth}
      \centering
      \begin{tikzpicture}[scale=1.5,circuit logic US]
        \node [and gate,point right,inputs=nn] (C) {$C$};
        \draw (C.input 1 -| -1,0) -- (C.input 1)
          node[at start]{\hspace{-1em}a};
        \draw (C.input 2 -| -1,0) -- (C.input 2)
          node[at start]{\hspace{-1em}b};
        \draw (C.output) -- ++(right:5mm) node[right]{$Q$};
      \end{tikzpicture}

      \column{.4\textwidth}
      \centering
      \begin{tabular}{cc|c}
        a & b & Q \\
        \hline
        0 & 0 & 0 \\
        0 & 1 & no change \\
        1 & 0 & no change \\
        1 & 1 & 1 \\
      \end{tabular}

    \end{columns}
  \end{frame}

  \begin{frame}{The Muller Pipeline}
    \includegraphics[width=\textwidth]{docs/pipeline.png}
    \cite{async}
  \end{frame}

  \begin{frame}[fragile]{Project: Asynchronous CPU}{Architecture}
    \pause
    \begin{verbatim}
      add, sub, lw, sw, li, jeq, jlt, jgt
    \end{verbatim}
    \pause
    \tikz\node{\includegraphics[width=\textwidth]{docs/diag-Design.pdf}};
  \end{frame}

  \begin{frame}{Next Steps}
    \pause
    \begin{itemize}
      \item Allow hardware synthesis\pause
      \item Synthesize on hardware\pause
      \item Use common CPU optimisation techniques\pause
      \item Run an OS
    \end{itemize}
  \end{frame}

  \begin{frame}
    \begin{center}
      \href{https://gitlab.com/CherryMan/async-cpu}
      {\color{blue}\underline{https://gitlab.com/CherryMan/async-cpu}}
    \end{center}
  \end{frame}

  \begin{frame}[t,allowframebreaks]{References}
    \nocite{*}
    \printbibliography
  \end{frame}
\end{document}
