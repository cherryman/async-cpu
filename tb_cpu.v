`include "rtl/cpu.v"

module tb_CPU;
    wire clk = 0;
    wire [15:0] io_i = 16'b0001000100010001;
    wire [15:0] io_o;

    reg rst;

    CPU cpu (.clk, .rst, .io_i, .io_o);

    initial begin
        $dumpfile("dump.vcd");
        $dumpvars(0, tb_CPU);

        rst = 1;
        #10 rst = 0;
        #200;
        $finish();
    end
endmodule
